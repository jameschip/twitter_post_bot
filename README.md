# twitter_post_bot

This is a really simple python script that uses tweepy to post tweets to an account. This used the V2 API so you do not need elevated access to use it. You do still have to turn on read and write in OAuth though.

You have to copy your keys into the correct fields for this to work.

make sure you have tweepy installed before you run this:

```
pip install tweepy
```

It reads a file called tweets.txt which is just a text file with a tweet on each line. It selects a random one and posts it.


# Schedule with cron (linux)

First make executable:

```
chmod +x twit.py
```

Now edit crontab

```
crontab -e
```

and insert this line:

```
* */2 * * * /home/$(USER)/twit.py
```

This will run the bot every two hours.
