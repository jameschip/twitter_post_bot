import tweepy
import random

# Open the Tweets file and pick a random line into a string.
access_token = 'x'
access_token_secret = 'x'
consumer_key = 'x'
consumer_secret = 'x'

tweet_content = random.choice(list(open('tweets.txt')))

client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)

response = client.create_tweet(
    text=tweet_content
)
print(f"https://twitter.com/user/status/{response.data['id']}")
    
print(response)
    

